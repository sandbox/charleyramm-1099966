Annotate module
by Charley Ramm

Allow users to make annotations (a type of note that only the user can see).
This might be useful for confidentially reviewing content. 

INSTALL
Add the containing directory to sites/all/modules and enable it in the settings/modules menu. 


